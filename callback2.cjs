const fs = require('fs')
const path = require('path')
/* 
    Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given list in lists.json. Then pass control back to the code that called it by using a callback function.
*/
function callback2(boardId, callbackFn) {
    setTimeout(() => {
        // Your code here
        fs.readFile(path.join(__dirname, 'lists.json'), 'utf-8', (err, list) => {
            if (err) {
                callbackFn(err)
            } else {
                console.log("read the file successfully")
                let listInfo = JSON.parse(list)[boardId]
                callbackFn(null, listInfo)
            }
        })
    }, 2 * 1000)
}

module.exports = callback2