const fs = require('fs')
const path = require('path')
/* 
    Problem 1: Write a function that will return a particular board's boardrmation based on the boardID that is passed from the given list of boards in boards.json and then pass control back to the code that called it by using a callback function.
*/
function callback1(boardId, callbackFn) {
    setTimeout(() => {
        // Your code here
        fs.readFile(path.join(__dirname, 'boards.json'), 'utf-8', (err, board) => {
            if (err) {
                callbackFn(err)
            } else {
                console.log("read the file successfully")
                let boardboard = JSON.parse(board).filter((board) => {
                    return board.id === boardId
                })
                callbackFn(null, boardboard)
            }
        })
    }, 2 * 1000)
}

module.exports = callback1