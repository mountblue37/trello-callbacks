const callback1 = require('./callback1.cjs')
const callback2 = require('./callback2.cjs')
const callback3 = require('./callback3.cjs')
/* 
    Problem 6: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for all lists simultaneously
*/
function callback6(boardId) {
    setTimeout(() => {

        callback1(boardId, (err, thanosInfo) => {
            if (err) {
                console.error(err)
            } else {
                console.log("Retrived thanos board info successfully")
                console.log(thanosInfo)
                const thanosId = thanosInfo[0].id

                callback2(thanosId, (err, thanosList) => {
                    if (err) {
                        console.error(err)
                    } else {
                        console.log("Retrived thanos list successfully")
                        console.log(thanosList)

                        let executeCounter = 0
                        thanosList.forEach(({ id, name }) => {
                            callback3(id, (err, cards) => {
                                if (err) {
                                    console.error(err)
                                } else {
                                    console.log("Retrived thanos cards successfully for " + name)
                                    if (cards) {
                                        console.log(cards)
                                    } else {
                                        console.log(`cards for ${name} doesn't exist`)
                                    }
                                    executeCounter++
                                    if (executeCounter === thanosList.length) {
                                        console.log("all operations done successfully")
                                    }
                                }
                            })
                        })
                    }
                })
            }
        })
    }, 2 * 1000)
}

module.exports = callback6