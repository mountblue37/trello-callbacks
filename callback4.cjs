const callback1 = require('./callback1.cjs')
const callback2 = require('./callback2.cjs')
const callback3 = require('./callback3.cjs')
/* 
    Problem 4: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind list simultaneously
*/
function callback4(boardId, infinityStone) {
    setTimeout(() => {

        callback1(boardId, (err, thanosInfo) => {
            if (err) {
                console.error(err)
            } else {
                console.log("Retrived thanos board info successfully")
                console.log(thanosInfo)
                const thanosId = thanosInfo[0].id

                callback2(thanosId, (err, thanosList) => {
                    if (err) {
                        console.error(err)
                    } else {
                        console.log("Retrived thanos list successfully")
                        console.log(thanosList)
                        const mindListId = thanosList.filter((stone) => {
                            return stone.name === infinityStone
                        })[0].id

                        callback3(mindListId, (err, cards) => {
                            if (err) {
                                console.error(err)
                            } else {
                                console.log("Retrived thanos cards successfully for " + infinityStone)
                                console.log(cards)
                            }
                        })
                    }
                })
            }
        })
    }, 2 * 1000)
}

module.exports = callback4