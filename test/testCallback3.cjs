const callback3 = require('../callback3.cjs')

function callbackFn(err, data) {
    if(err) {
        console.error(err)
    } else {
        console.log(data)
    }
}

callback3('qwsa221', callbackFn)