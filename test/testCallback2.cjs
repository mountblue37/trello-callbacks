const callback2 = require('../callback2.cjs')

function callbackFn(err, data) {
    if(err) {
        console.error(err)
    } else {
        console.log(data)
    }
}

callback2('abc122dc', callbackFn)