const callback1 = require('../callback1.cjs')

function callbackFn(err, data) {
    if(err) {
        console.error(err)
    } else {
        console.log(data)
    }
}

callback1('mcu453ed', callbackFn)