const callback1 = require('./callback1.cjs')
const callback2 = require('./callback2.cjs')
const callback3 = require('./callback3.cjs')
/* 
    Problem 5: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind and Space lists simultaneously
*/
function callback5(boardId, infinityStone1, infinityStone2) {
    setTimeout(() => {

        callback1(boardId, (err, thanosInfo) => {
            if (err) {
                console.error(err)
            } else {
                console.log("Retrived thanos board info successfully")
                console.log(thanosInfo)
                const thanosId = thanosInfo[0].id

                callback2(thanosId, (err, thanosList) => {
                    if (err) {
                        console.error(err)
                    } else {
                        console.log("Retrived thanos list successfully")
                        console.log(thanosList)
                        const mindListIds = thanosList.filter((stone) => {
                            return stone.name === infinityStone1 || stone.name === infinityStone2
                        })

                        let executeCounter = 0
                        mindListIds.forEach(({ id, name }) => {
                            callback3(id, (err, cards) => {
                                if (err) {
                                    console.error(err)
                                } else {
                                    console.log("Retrived thanos cards successfully for " + name)
                                    if (cards) {
                                        console.log(cards)
                                    } else {
                                        console.log(`cards for ${name} doesn't exist`)
                                    }
                                    executeCounter++
                                    if (executeCounter === mindListIds.length) {
                                        console.log("all operations done successfully")
                                    }
                                }
                            })
                        })
                    }
                })
            }
        })
    }, 2 * 1000)
}

module.exports = callback5