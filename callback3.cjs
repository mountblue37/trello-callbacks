const fs = require('fs')
const path = require('path')
/* 
    Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it from the given data in cards.json. Then pass control back to the code that called it by using a callback function.
*/
function callback3(listId, callbackFn) {
    setTimeout(() => {
        // Your code here
        fs.readFile(path.join(__dirname, 'cards.json'), 'utf-8', (err, card) => {
            if (err) {
                callbackFn(err)
            } else {
                console.log("read the file successfully")
                let cardInfo = JSON.parse(card)[listId]
                callbackFn(null, cardInfo)
            }
        })
    }, 2 * 1000)
}

module.exports = callback3